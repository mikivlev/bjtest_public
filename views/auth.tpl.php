	<div class="container table-block">
		<div class="row table-cell-block">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<h1 class="text-center login-title">Авторизация</h1>
				<div class="account-wall">
					<form action="/auth/authCheck" class="form-signin" id="form-signin" method="post">
						<?php if(!empty($pageData['error'])) :?>
							<p><?php echo $pageData['error']; ?></p>
						<?php endif; ?>
					<input type="text" class="form-control" name="login" id="login" placeholder="Логин">
					<input type="password" name="password" id="password" class="form-control" placeholder="Пароль">
					
					<button class="btn btn-lg btn-primary" type="submit">Войти</button>
					<a href="/" class="btn btn-lg btn-primary">Назад</a>
					</form>
				</div>
			</div>
		</div>
	</div>