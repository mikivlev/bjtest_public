<div id="content">

	<div class="panel-body">
		<?php 
			if(isset($_SESSION['admin'])){
				echo ('<p>Вы зашли как ' . $_SESSION['admin'] . '</p>');
				echo ('<a href="logout" class="btn btn-sm btn-primary">выход</a>');
			} else {
				echo ('<a href="auth" class="btn btn-lg btn-primary">Авторизация</a><br>');
			}
		?>
		<p></p>
		<div class="row">
			<div class="col-lg-12">
				<div class="table-responsive">
					<form action="saveTable" method="post">
					
						<table class="table table-bordered table-hover table-striped">
							<thead>
								<tr>
									<th class="shth shth-name">Имя пользователя
										<a href="?page=<?php echo($pageData['currentPage'])?>&sort=1&asc=1" class="btn btn-sm btn-primary btn-sort">a-z</a>
										<a href="?page=<?php echo($pageData['currentPage'])?>&sort=1&asc=0" class="btn btn-sm btn-primary btn-sort">z-a</a>
									</th>
									<th class="shth shth-mail">E-mail
										<a href="?page=<?php echo($pageData['currentPage'])?>&sort=2&asc=1" class="btn btn-sm btn-primary btn-sort">a-z</a>
										<a href="?page=<?php echo($pageData['currentPage'])?>&sort=2&asc=0" class="btn btn-sm btn-primary btn-sort">z-a</a>
									</th>
									<th class="shth shth-text">Текст задачи</th>
									<th class="shth shth-stat">Статус
										<a href="?page=<?php echo($pageData['currentPage'])?>&sort=3&asc=1" class="btn btn-sm btn-primary btn-sort">a-z</a>
										<a href="?page=<?php echo($pageData['currentPage'])?>&sort=3&asc=0" class="btn btn-sm btn-primary btn-sort">z-a</a>
									</th>
									<?php if(isset($_SESSION['admin'])){echo('<th class="shth shth-done">Выполнено</th>');}?>
								</tr>
							</thead>
							<tbody>
								<?php

									for($i=$pageData['firstEntry']; $i< ($pageData['firstEntry'] + $pageData['rowsqty']); $i++){
										if(isset($pageData['jobs'][$i])){
										echo '<tr class="shtr">';
											echo '<td>' . $pageData['jobs'][$i]['username'] . '</td>';
											echo '<td>' . $pageData['jobs'][$i]['email'] . '</td>';

											if(isset($_SESSION['admin'])){
												echo('<td>
												<input class="form-control" type="text" name="jobtextinput[]" value="' . $pageData['jobs'][$i]['jobtext'] . '">
												<input type="hidden" name="idinput[]" value="' . $pageData['jobs'][$i]['id'] . '">
												</td>');
											} else {
												echo('<td>' . $pageData['jobs'][$i]['jobtext'] . '</td>');
											}
											$status = ($pageData['jobs'][$i]['status'])?'выполнено':'';
											echo '<td>' . $status . '</td>';

											if(isset($_SESSION['admin'])){
												echo('<td><input type="checkbox" 
												name="checkinput_' . $pageData['jobs'][$i]['id'] . 
												'"');
												if($pageData['jobs'][$i]['status']) echo(' checked');
												echo('></td>');
											}

											echo '</tr>';
										} else{
											echo ('<tr class="shtr"><td><td><td><td>');
											if(isset($_SESSION['admin'])){echo('<td>');};
											echo ('</tr>');
										}
									}
								?>

							</tbody>
						</table>
						<?php 
							if($pageData['jobs']){
								for ($i = 1; $i <= $pageData['pagesTotal']; $i++){
									echo '<a class="btn btn-sm btn-primary btn-pag" 
									href="?page=' . $i . '&sort=' . $pageData['sortMethod'] . 
									'&asc=' . $pageData['sortAscending'] . '">' . $i . ' </a>';
								}
							} else {
								echo ('<a class="btn btn-sm btn-primary btn-pag" href="/">1</a>');
							}
						?>
						<p></p>
						<?php 
							if(isset($_SESSION['admin'])){
								echo('<button class="btn btn-lg btn-primary btn-adminsave" type="submit">Сохранить</button>');
							}
						?>
							
					</form>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.col-lg-4 (nested) -->
			<!-- /.col-lg-8 (nested) -->
		</div>
		<!-- /.row -->
	<a href="newjob" class="btn btn-lg btn-primary" type="submit">Новая задача</a>
	</div>
	<!-- /.panel-body -->
</div>