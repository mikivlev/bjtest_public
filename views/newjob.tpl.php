	<div class="container table-block">
		<div class="row table-cell-block">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				
				<div class="account-wall">
					<h1 class="text-center login-title">Новая задача</h1>
					<form action="/newjob/addData" class="form-signin" id="form-signin" method="post">
						<?php if(!empty($pageData['job']['message'])) :?>
							<p><?php echo $pageData['job']['message']; ?></p>
						<?php endif; ?>
						<input type="text" class="form-control" name="username" id="username" placeholder="Имя">
						<input type="text" class="form-control" name="email" id="email" placeholder="Почта">
						<input type="text-btm" class="form-control" name="jobtext" id="jobtext"  placeholder="Текст">
						<a href="/" class="btn btn-lg btn-primary btn-block">Назад</a>
						<button class="btn btn-lg btn-primary btn-block" type="submit">Сохранить</button>
					</form>
				</div>
			</div>
		</div>
	</div>