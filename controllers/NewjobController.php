<?php

class NewjobController extends Controller {

	private $pageTpl = VIEW_PATH. 'newjob.tpl.php';

	public function __construct() {
		$this->model = new NewjobModel();
		$this->view = new View();
	}

	public function index() {
		$this->pageData['title'] = 'Новая задача';
		$this->view->render($this->pageTpl, $this->pageData);
	}

	public function addData(){
		$this->pageData['title'] = 'Новая задача';
		$this->pageData['job'] = $this->model->validateFields();
		if($this->pageData['job']['validate']){
			$this->model->addJobToDb($this->pageData['job']);
		}
		$this->view->render($this->pageTpl, $this->pageData);
	}

}
