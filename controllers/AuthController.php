<?php

class AuthController extends Controller {

	private $pageTpl = VIEW_PATH. 'auth.tpl.php';

	public function __construct() {
		$this->model = new AuthModel();
		$this->view = new View();
	}

	public function index() {
		$this->pageData['title'] = 'Авторизация';
		$this->view->render($this->pageTpl, $this->pageData);
	}

	public function authCheck(){
		$this->pageData['title'] = 'Авторизация';
		$this->pageData['error'] = $this->model->validateAuth();
		$this->view->render($this->pageTpl, $this->pageData);
	}

}