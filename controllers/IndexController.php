<?php

class IndexController extends Controller {

	private $pageTpl = VIEW_PATH. 'index.tpl.php';

	public function __construct() {
		$this->model = new IndexModel();
		$this->view = new View();
	}

	public function index() {
		$this->pageData['title'] = "Список задач";
		$this->pageData['rowsqty'] = 3;
		$this->pageData['sortMethod'] = $this->model->getSortMethod();
		$this->pageData['sortAscending'] = $this->model->getSortAscending();
		$this->pageData['currentPage'] = $this->model->getCurrentPage();
		$this->pageData['firstEntry'] = $this->pageData['currentPage'] * $this->pageData['rowsqty'] - $this->pageData['rowsqty'];

		$this->pageData['jobs'] = $this->model->getDbTable('jobs' , $this->pageData['sortMethod'] , $this->pageData['sortAscending']);

		if($this->pageData['jobs']){
			$this->pageData['entriesTotal'] = count($this->pageData['jobs']);
			$this->pageData['pagesTotal'] = ceil($this->pageData['entriesTotal'] / $this->pageData['rowsqty']);
		}

		$this->view->render($this->pageTpl, $this->pageData);

	}

	public function logout(){
		session_destroy();
		header('Location: /');
	}

	public function saveTable(){
		if(!($_SESSION['admin'])){
			header('Location: /auth');
			exit;
		}

		foreach($_POST['idinput'] as $key => $value){
			$this->model->updateJobText($value, $_POST['jobtextinput'][$key]); 
		}

		foreach($_POST['idinput'] as $id){
			if(isset($_POST['checkinput_' . $id])){
				$this->model->updateJobStatus($id, true);
			} else {
				$this->model->updateJobStatus($id, false);
			}
		}
		header('Location: /');
	}
}