<?php
class DB{
	const HOST = 'localhost';
	const DB   = 'a0172224_bjdb';
	const USER = 'a0172224_bjdb';
	const PASS = 'test';

	public static function dbConnect(){

		$user = self::USER;
		$pass = self::PASS;
		$host = self::HOST;
		$db   = self::DB;

		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];

		$conn = new PDO("mysql:dbname=$db;host=$host;charset=UTF8", $user, $pass, $opt);
		return $conn;

	}


}