<?php
class Model {
	protected $db = null;

	public function __construct() {
		$this->db = DB::dbConnect();
	}

	public function getDbTable($tbl, $sort , $asc){

		($asc) ? $dbasc = 'ASC' : $dbasc = 'DESC';
		switch($sort){
			case 0:
				$dbsort = '';
				break;
			case 1:
				$dbsort = 'username';
				break;
			case 2:
				$dbsort = 'email';
				break;
			case 3:
				$dbsort = 'status';
				break;
			default:
				$dbsort = '';
				break;
		}

		if($dbsort != ''){
			$sql = 'SELECT * FROM ' . $tbl . ' ORDER BY ' . $dbsort . ' ' . $dbasc;
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
		} else{
			$sql = 'SELECT * FROM ' . $tbl;
			$stmt = $this->db->prepare($sql);
			$stmt->execute();
		}

		$res = array();
		while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			array_push($res, $row);
		}

		if(!empty($res)) {
			return $res;
		} else {
			return false;
		}
	}

	public function getSafePostParam($postParam){
		if($postParam == '') return $postParam;
		$res = trim(urldecode(htmlspecialchars($postParam)));
		if (isset($res)){return $res;}else {return '';}
	}

}