<?php
class NewjobModel extends Model {

	public function validateFields(){

		$job = array();

		(isset($_POST['username'])) ? $job['username'] = $this->getSafePostParam($_POST['username']) : $job['username'] = '';
		(isset($_POST['email'])) ? $job['email'] = $this->getSafePostParam($_POST['email']) : $job['email'] = '';
		(isset($_POST['jobtext'])) ? $job['jobtext'] = $this->getSafePostParam($_POST['jobtext']) : $job['jobtext'] = '';

		if(($job['username'] == '' || $job['email'] == '' || $job['jobtext'] == '')){
			$job['validate'] = false;
			$job['message'] = 'Вы заполнили не все поля';
		}elseif(!filter_var($job['email'], FILTER_VALIDATE_EMAIL)){
			$job['validate'] = false;
			$job['message'] = 'Email адрес указан неверно';
		} else{
			$job['validate'] = true;
			$job['message'] = 'Задача успешно добавлена';
		}

		return $job;

	}

	public function addJobToDb($params){
		$sql = "INSERT INTO jobs (username, jobtext, email) VALUES (?, ?, ?)";
		$stmt = $this->db->prepare($sql);
		$stmt->bindValue(1, $params['username']);
		$stmt->bindValue(2, $params['jobtext']);
		$stmt->bindValue(3, $params['email']);
		$stmt->execute();
	}
}