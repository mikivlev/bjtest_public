<?php

class IndexModel extends Model {

	public function getCurrentPage(){
		if (isset($_GET['page'])){
			return $_GET['page'];
		}else return 1;
	}

	public function getSortMethod(){
		if (isset($_GET['sort'])){
			return $_GET['sort'];
		}else return 0;
	}

	public function getSortAscending(){
		if (isset($_GET['asc'])){
			return $_GET['asc'];
		}else return 0;
	}

	public function updateJobText($id, $txt){

		$sql = "SELECT `jobtext` FROM `jobs` WHERE `jobs`.`id` = :id";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$compare = $stmt->fetch()['jobtext'];

		if($txt != $compare){
			$jobtext = $txt . ' (отредактировано администратором)';
			$sql = "UPDATE `jobs` SET `jobtext` = :jobtext WHERE `jobs`.`id` = :id";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':jobtext', $jobtext);
			$stmt->bindParam(':id', $id);
			$stmt->execute();
		}

	}

	public function updateJobStatus($id, $status){
		$sql = "UPDATE `jobs` SET `status` = :status WHERE `jobs`.`id` = :id";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':status', $status);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}
}