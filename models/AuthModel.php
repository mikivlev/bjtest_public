<?php
class AuthModel extends Model {

	public function validateAuth(){

		(isset($_POST['login'])) ? $login = $this->getSafePostParam($_POST['login']) : $login = '';
		(isset($_POST['password'])) ? $password = md5($this->getSafePostParam($_POST['password'])) : $password = '';

		if($login == '' || $password == ''){
			return 'Вы заполнили не все поля';
		} else {
			$sql = "SELECT * FROM users WHERE login = :login AND password = :password";
			$stmt = $this->db->prepare($sql);
			$stmt->bindValue(':login', $login, PDO::PARAM_STR);
			$stmt->bindValue(':password', $password, PDO::PARAM_STR);
			$stmt->execute();

			$res = $stmt->fetch(PDO::FETCH_ASSOC);

			if(!empty($res)) {
				$_SESSION['admin'] = $_POST['login'];
				$this->pageData['isAdmin'] = true;
				header('Location: /');
			} else{
				return 'Неправильный логин или пароль';
			}

		}

	}
}